# ChainReaction

http://blakebrauer.gitlab.io/ChainReaction/

A chain reaction JavaScript game.

Click on a tile to turn it. If the newly rotated tile connects with a neighboring tile, the neighboring tile then turns. Try to beat your high score!
